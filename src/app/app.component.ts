import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  date = new Date().getDate();
  today = new Date();
  month = this.today.toLocaleString('default', { month: 'long' });

  datas = [
    { id: 1, day: 13, month: "April", dayName: "Seshanba", fajr: "04 : 25", maghrib: "19 : 03" },
    { id: 2, day: 14, month: "April", dayName: "Chorshanba", fajr: "04 : 24", maghrib: "19 : 04" },
    { id: 3, day: 15, month: "April", dayName: "Payshanba", fajr: "04 : 22", maghrib: "19 : 05" },
    { id: 4, day: 16, month: "April", dayName: "Juma", fajr: "04 : 20", maghrib: "19 : 06" },
    { id: 5, day: 17, month: "April", dayName: "Shanba", fajr: "04 : 18", maghrib: "19 : 07" },
    { id: 6, day: 18, month: "April", dayName: "Yankshanba", fajr: "04 : 16", maghrib: "19 : 08" },
    { id: 7, day: 19, month: "April", dayName: "Dushanba", fajr: "04 : 14", maghrib: "19 : 09" },
    { id: 8, day: 20, month: "April", dayName: "Seshanba", fajr: "04 : 13", maghrib: "19 : 10" },
    { id: 9, day: 21, month: "April", dayName: "Chorshanba", fajr: "04 : 11", maghrib: "19 : 11" },
    { id: 10, day: 22, month: "April", dayName: "Payshanba", fajr: "04 : 09", maghrib: "19 : 13" },
    { id: 11, day: 23, month: "April", dayName: "Juma", fajr: "04 : 07", maghrib: "19 : 14" },
    { id: 12, day: 24, month: "April", dayName: "Shanba", fajr: "04 : 05", maghrib: "19 : 15" },
    { id: 13, day: 25, month: "April", dayName: "Yakshanba", fajr: "04 : 04", maghrib: "19 : 16" },
    { id: 14, day: 26, month: "April", dayName: "Dushanba", fajr: "04 : 02", maghrib: "19 : 17" },
    { id: 15, day: 27, month: "April", dayName: "Seshanba", fajr: "04 : 00", maghrib: "19 : 18" },
    { id: 16, day: 28, month: "April", dayName: "Chorshanba", fajr: "03 : 58", maghrib: "19 : 19" },
    { id: 17, day: 29, month: "April", dayName: "Payshanba", fajr: "03 : 47", maghrib: "19 : 20" },
    { id: 18, day: 30, month: "April", dayName: "Juma", fajr: "03 : 55", maghrib: "19 : 21" },
    { id: 19, day: 1, month: "May", dayName: "Shanba", fajr: "03 : 53", maghrib: "19 : 22" },
    { id: 20, day: 2, month: "May", dayName: "Yakshanba", fajr: "03 : 51", maghrib: "19 : 23" },
    { id: 21, day: 3, month: "May", dayName: "Dushanba", fajr: "03 : 50", maghrib: "19 : 24" },
    { id: 22, day: 4, month: "May", dayName: "Seshanba", fajr: "03 : 48", maghrib: "19 : 25" },
    { id: 23, day: 5, month: "May", dayName: "Chorshanba", fajr: "03 : 46", maghrib: "19 : 26" },
    { id: 24, day: 6, month: "May", dayName: "Payshanba", fajr: "03 : 45", maghrib: "19 : 27" },
    { id: 25, day: 7, month: "May", dayName: "Juma", fajr: "03 : 43", maghrib: "19 : 28" },
    { id: 26, day: 8, month: "May", dayName: "Shanba", fajr: "03 : 42", maghrib: "19 : 29" },
    { id: 27, day: 9, month: "May", dayName: "Yakshanba", fajr: "03 : 40", maghrib: "19 : 31" },
    { id: 28, day: 10, month: "May", dayName: "Dushanba", fajr: "03 : 39", maghrib: "19 : 32" },
    { id: 29, day: 11, month: "May", dayName: "Seshanba", fajr: "03 : 37", maghrib: "19 : 33" },
    { id: 30, day: 12, month: "May", dayName: "Chorshanba", fajr: "03 : 36", maghrib: "19 : 34" },
  ]
}
