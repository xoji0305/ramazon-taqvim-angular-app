const data = [
    { id: 1, day: 13, dayName: "Seshanba", fajr: "04 : 25", maghrib: "19 : 03" },
    { id: 2, day: 14, dayName: "Chorshanba", fajr: "04 : 24", maghrib: "19 : 04" },
    { id: 3, day: 15, dayName: "Payshanba", fajr: "04 : 22", maghrib: "19 : 05" },
    { id: 4, day: 16, dayName: "Juma", fajr: "04 : 20", maghrib: "19 : 06" },
    { id: 5, day: 17, dayName: "Shanba", fajr: "04 : 18", maghrib: "19 : 07" },
]